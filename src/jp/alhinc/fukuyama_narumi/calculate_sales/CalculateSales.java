package jp.alhinc.fukuyama_narumi.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {

		Map<String, String> storeMap = new HashMap<String, String>();
		Map<String, Long> salesMap = new HashMap<String, Long>();

		BufferedReader br = null;
		try {
			File file = new File(args[0], "branch.lst");
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			if (!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}

			String line;
			while ((line = br.readLine()) != null) {
				if (!line.matches("^[0-9]{3},[^,]+")) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				//データ読み込み、保持
				String[] storeName = line.split(",");
				String storeCode = storeName[0];
				storeMap.put(storeCode, storeName[1]);
				salesMap.put(storeCode, 0L);

			}

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;

		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}

		//2,集計 (ファイル検索 rcdかつファイル名が数字8桁）
		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File file, String str) {
				if (str.matches("[0-9]{8}\\.(rcd)$")) { //完全一致検索
					return true;
				} else {
					return false;
				}
			}
		};
		//ファイル読込
		File[] list = new File(args[0]).listFiles(filter);

		String minIndex = list[0].getName().substring(0, 8);
		String maxIndex = list[list.length - 1].getName().substring(0, 8);
		int min = Integer.parseInt(minIndex);
		int max = Integer.parseInt(maxIndex);

		for (int index = 0; index < list.length; index++) {

			if (min + (list.length - 1) != max) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		//売上ファイル読み込み 
		for (int index = 0; index < list.length; index++) {
			br = null;
			try {
				FileReader fr = new FileReader(list[index]);
				br = new BufferedReader(fr);


				String storeCode = br.readLine();
				String storeSale = br.readLine();
				if (br.readLine() != null) {
					System.out.println(list[index].getName() + "のフォーマットが不正です");
					return;
				}

				if (!storeMap.containsKey(storeCode)) {
					System.out.println(list[index].getName() + "の支店コードが不正です");
					return;
				

				}else{
					long sales = Long.parseLong(storeSale);
					salesMap.put(storeCode, salesMap.get(storeCode) + (sales));
				}

				if(salesMap.get(storeCode) >9999999999L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				
			} catch (Exception e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}

		}

		BufferedWriter bw = null;
		try {
			File outFile = new File(args[0], "branch.out");
			FileWriter fw = new FileWriter(outFile);
			bw = new BufferedWriter(fw);
			for (String str : salesMap.keySet()) {
				bw.write(str + "," + storeMap.get(str) + "," + salesMap.get(str)); //ファイル出力
				bw.newLine();
			}
		} catch (Exception e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}

	}

}
